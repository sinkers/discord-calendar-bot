const { Pool } = require('pg')
const pool = new Pool()

pool.on('error', (err) => {
  console.error('Unexpected error on idle client', err)
  process.exit(-1)
})


const request = async (request) => {
  const client = await pool.connect()
  try {
    // return await client.query('SELECT * FROM users WHERE di_id = $1', ["sinkers"])
    return await client.query(request)
  } finally {
    client.release()
  }
}

const createUser = async ({id, name, email}) => {
  try {
    await request({text: `
      INSERT INTO users VALUES ($1, $2, $3)
      ON CONFLICT (di_id) DO UPDATE
        SET 
          di_name = EXCLUDED.di_name,
          email = EXCLUDED.email
    `, values: [id, name, email]
  });
  } catch (e) {
    if (e.message === `duplicate key value violates unique constraint "users_email_key"`) {
      throw new Error("Email уже используется")
    } else throw e;
  }
};

const getUserList = async (emails) => {
  const response = await request({
    text: `SELECT di_id as discord_id, di_name as discord_name, email FROM users${emails && ` WHERE email = ANY($1)` || ``};`,
    values: emails && [emails]
  });
  return response.rows;
};

module.exports = {
  createUser,
  getUserList
}
