const fs = require("fs").promises;
const { google } = require("googleapis");
const { parse, format, setSeconds, addMinutes, addSeconds } = require("date-fns")

const GOOGLE_API_KEY = process.env.GOOGLE_API_KEY;
const GOOGLE_CALENDAR_ID = process.env.GOOGLE_CALENDAR_ID;
const SCOPE_CALENDAR = 'https://www.googleapis.com/auth/calendar'; // authorization scopes
const SCOPE_EVENTS = 'https://www.googleapis.com/auth/calendar.events';

const DATE_FORMAT = "dd.MM.yy HH:mm";

const readPrivateKey = async () => {
  const content = await fs.readFile(GOOGLE_API_KEY);
  return JSON.parse(content.toString());
}

const authenticate = async (key) => {
  const jwtClient = new google.auth.JWT(
    key.client_email,
    null,
    key.private_key,
    [SCOPE_CALENDAR, SCOPE_EVENTS]
  );
  await jwtClient.authorize();
  return jwtClient;
}
const GoogleCalendar = google.calendar('v3');
let auth = null;
const getAuth = async () =>  {
  return auth || await authenticate(await readPrivateKey());
}
const addEventToCalendar = async ({name, description, participants, startTime, endTime, place, type}) => {
  const event = {
    summary: name,
    description: `${description}
место проведения: ${place}
тип мероприятия: ${type}`,
    start: {
      dateTime: startTime.toISOString(),
    },
    end: {
      dateTime: endTime.toISOString(),
    },
    attendees: participants && participants.map(email => ({email})) || undefined,
  };

  return await GoogleCalendar.events.insert({
    auth: await getAuth(),
    calendarId: GOOGLE_CALENDAR_ID,
    requestBody: event,
  });
}

const createEvent = async ({name, description, participants, startTime, endTime, place, type}) => {
  startTime = parse(startTime, DATE_FORMAT, new Date());
  endTime = parse(endTime, DATE_FORMAT, new Date());
  participants = participants && participants.split(" ");
  return await addEventToCalendar({name, description, participants, startTime, endTime, place, type})
}

const getUpcomingEvents = async (statTime = new Date(), offset = 10) => {
  const timeMin = setSeconds(statTime, 0)
  const timeMax = addSeconds(addMinutes(timeMin, offset), 1);

  const response = await GoogleCalendar.events.list({
    auth: await getAuth(),
    calendarId: GOOGLE_CALENDAR_ID,
    timeMin,
    timeMax
  });
  const { items } = response.data;
  return items.map(item => {
    const place = /место проведения: (.*)/gm.exec(item.description)[1];
    const type = /тип мероприятия: (.*)/gm.exec(item.description)[1];
    return {
      name: item.summary,
      description: item.description,
      participants: item.attendees && item.attendees.map(a => a.email) || [],
      startTimeFormatted: format(new Date(item.start.dateTime), DATE_FORMAT),
      endTimeFormatted: format(new Date(item.end.dateTime), DATE_FORMAT),
      startTime: new Date(item.start.dateTime),
      endTime: new Date(item.end.dateTime),
      place,
      type
    }
  })
}

module.exports = {
  createEvent,
  getUpcomingEvents
}
