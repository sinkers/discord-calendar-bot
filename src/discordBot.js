const sql = require("./sql");
const messages = require("./messages");
const { Client, DMChannel } = require("discord.js");
const { createEvent, getUpcomingEvents  } = require("./events");
const { addMinutes, setSeconds, isEqual, setMilliseconds } = require("date-fns");

const TOKEN = process.env.DISCORD_BOT;
const client = new Client();

const COMMANDS = {
  HELP: "помощь",
  REGISTER_USER: "регистрация",
  CREATE_EVENT: "создать"
}

const EVENT_TYPES = {
  LOCAL: "local",
  REMOTE: "remote"
}
const START_SEPARATOR = " ";
const SEPARATOR = ", ";

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', async msg => {
  if (msg.content.toLowerCase() === COMMANDS.HELP.toLowerCase()) {
    messages.HELP.MESSAGE.forEach(m => msg.reply(m))
  } else if (msg.channel instanceof DMChannel) {
    // DM message, should manage events
    if (msg.content.startsWith(COMMANDS.REGISTER_USER)) {
      const [,email] = msg.content.split(COMMANDS.REGISTER_USER + START_SEPARATOR)
      if (!email) {
        messages.REGISTRATION.EMAIL_REQUIRED.forEach(m => msg.reply(m))
        return;
      }
      try {
        await sql.createUser({id: msg.author.id, name: msg.author.tag, email})
        messages.REGISTRATION.SUCCESS.forEach(m => msg.reply(m));
      } catch (e) {
        messages.REGISTRATION.ERROR.forEach((m, i) => msg.reply(i === 0 ? `${m} ${e.message}` : m));
      }
    } else if (msg.content.startsWith(COMMANDS.CREATE_EVENT)) {
      const [,commandBody] = msg.content.split(COMMANDS.CREATE_EVENT + START_SEPARATOR);
      const [name, description, participants, startTime, endTime, place,] = commandBody.split(SEPARATOR)
      let type = EVENT_TYPES.REMOTE
      if (place.toLowerCase().startsWith("офис")) {
        type = EVENT_TYPES.LOCAL
      }
      try {
        await createEvent({name, description, participants, startTime, endTime, place, type})
        messages.EVENTS.SUCCESS.forEach(m => msg.reply(m));
      } catch (e) {
        console.log("create event error", e);
      }
    }
  }
});

let timeout;
const checkEvents = async () => {
  clearTimeout(timeout);
  const currentDate = setSeconds(setMilliseconds(new Date(), 0), 0);
  const tenMinutesAfter = addMinutes(currentDate, 10);
  const fiveMinutesAfter = addMinutes(currentDate, 5);
  const events = await getUpcomingEvents();
  for (const event of events) {
    let title;
    if (isEqual(event.startTime, tenMinutesAfter)) {
      // 10 minutes before event
      title = "Через 10 минут начнется мероприятие!";
    } else if (isEqual(event.startTime, fiveMinutesAfter)) {
      // 5 minutes before event
      title = "Через 5 минут начнется мероприятие!"
    } else if (isEqual(event.startTime, currentDate)) {
      // event started
      title = "Начинается мероприятие!"
    }
    console.log(event, title, currentDate,tenMinutesAfter, fiveMinutesAfter)

    if (title) {
      let users = event.participants && await sql.getUserList(event.participants) || []
      const message = `${title}
Название: ${event.name}
Описание: ${event.description}
Место проведения: ${event.place}
тип: ${event.type}
`;
      for (const user of users) {
        try {
          if (user.discord_id) {
            const discordUser = await client.users.fetch(user.discord_id);
            await discordUser.send(message);
          }
        } catch (e) {
          console.error("Can't send notification", e)
        }
      }
    }
   timeout = setTimeout(() => checkEvents(), 60 * 1000);
  }
}

console.log("Bot login")
client.login(TOKEN).then(() => {
  console.log("bot ready")
  checkEvents().catch(e => console.error(e));
}).catch(e => {
  console.dir(e);
  process.exit(-1);
});


